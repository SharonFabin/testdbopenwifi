version: '3'

volumes:
  owgw_data:
    driver: local
  owsec_data:
    driver: local
  owfms_data:
    driver: local
  owprov_data:
    driver: local
  zookeeper_data:
    driver: local
  zookeeper_datalog:
    driver: local
  kafka_data:
    driver: local

networks:
  openwifi:

services:
  owgw:
    image: "tip-tip-wlan-cloud-ucentral.jfrog.io/owgw:${OWGW_TAG}"
    networks:
      openwifi:
        aliases:
          - ${INTERNAL_OWGW_HOSTNAME}
    env_file:
      - .env.selfsigned
      - owgw.env
    depends_on:
      - kafka
      - rttys
    restart: unless-stopped
    volumes:
      - owgw_data:${OWGW_ROOT}/persist
      - ./certs:/${OWGW_ROOT}/certs
    sysctls:
      - net.ipv4.tcp_keepalive_intvl=5
      - net.ipv4.tcp_keepalive_probes=2
      - net.ipv4.tcp_keepalive_time=45

  owgw-ui:
    image: "tip-tip-wlan-cloud-ucentral.jfrog.io/owgw-ui:${OWGWUI_TAG}"
    env_file: 
      - owgw-ui.env
    networks:
      openwifi:
        aliases:
          - ${INTERNAL_OWGWUI_HOSTNAME}
    env_file:
      - owgw-ui.env
    depends_on:
      - owsec
      - owgw
      - owfms
      - owprov
    restart: unless-stopped

  owsec:
    image: "tip-tip-wlan-cloud-ucentral.jfrog.io/owsec:${OWSEC_TAG}"
    networks:
      openwifi:
        aliases:
          - ${INTERNAL_OWSEC_HOSTNAME}
    env_file:
      - .env.selfsigned
      - owsec.env
    depends_on:
      - kafka
    restart: unless-stopped
    volumes:
      - owsec_data:${OWSEC_ROOT}/persist
      - ./certs:/${OWSEC_ROOT}/certs

  owfms:
    image: "tip-tip-wlan-cloud-ucentral.jfrog.io/owfms:${OWFMS_TAG}"
    networks:
      openwifi:
        aliases:
          - ${INTERNAL_OWFMS_HOSTNAME}
    env_file:
      - .env.selfsigned
      - owfms.env
    depends_on:
      - kafka
    restart: unless-stopped
    volumes:
      - owfms_data:${OWFMS_ROOT}/persist
      - ./certs:/${OWFMS_ROOT}/certs

  owprov:
    image: "tip-tip-wlan-cloud-ucentral.jfrog.io/owprov:${OWPROV_TAG}"
    networks:
      openwifi:
        aliases:
          - ${INTERNAL_OWPROV_HOSTNAME}
    env_file:
      - .env.selfsigned
      - owprov.env
    depends_on:
      - kafka
    restart: unless-stopped
    volumes:
      - owprov_data:${OWPROV_ROOT}
      - ./certs:/${OWPROV_ROOT}/certs

  owprov-ui:
    image: "tip-tip-wlan-cloud-ucentral.jfrog.io/owprov-ui:${OWPROVUI_TAG}"
    networks:
      openwifi:
        aliases:
          - ${INTERNAL_OWPROVUI_HOSTNAME}
    env_file:
      - owprov-ui.env
    depends_on:
      - owsec
      - owgw
      - owfms
      - owprov
    restart: unless-stopped

  rttys:
    image: "tip-tip-wlan-cloud-ucentral.jfrog.io/rttys:${RTTYS_TAG}"
    networks:
      openwifi:
        aliases:
          - ${INTERNAL_RTTYS_HOSTNAME}
    restart: unless-stopped
    volumes:
      - "./certs/restapi-cert.pem:/etc/rttys/restapi-cert.pem"
      - "./certs/restapi-key.pem:/etc/rttys/restapi-key.pem"
      - "./rttys/rttys.conf:/rttys/rttys.conf"

  zookeeper:
    image: "zookeeper:${ZOOKEEPER_TAG}"
    networks:
      openwifi:
    restart: unless-stopped
    volumes:
      - zookeeper_data:/data
      - zookeeper_datalog:/datalog

  kafka:
    image: "docker.io/bitnami/kafka:${KAFKA_TAG}"
    networks:
      openwifi:
    env_file:
      - kafka.env
    restart: unless-stopped
    depends_on:
      - zookeeper
    volumes:
      - kafka_data:/bitnami/kafka

  traefik:
    image: "traefik:${TRAEFIK_TAG}"
    networks:
      openwifi:
    env_file:
      - traefik.env
    depends_on:
      - owsec
      - owgw
      - owgw-ui
      - owfms
      - owprov
      - owprov-ui
      - rttys
    restart: unless-stopped
    volumes:
      - "./traefik/openwifi_selfsigned.yaml:/etc/traefik/openwifi.yaml"
      - "./certs/restapi-ca.pem:/certs/restapi-ca.pem"
      - "./certs/restapi-cert.pem:/certs/restapi-cert.pem"
      - "./certs/restapi-key.pem:/certs/restapi-key.pem"
    ports:
      - "15002:15002"
      - "16002:16002"
      - "16003:16003"
      - "80:80"
      - "8080:8080"
      - "443:443"
      - "8443:8443"
      - "16001:16001"
      - "16004:16004"
      - "16005:16005"
      - "5912:5912"
      - "5913:5913"
